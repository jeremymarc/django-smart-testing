# How to Contribute

There are many ways you can help contribute to django-smart-testing.

Contributing code or tests, writing documentation, reporting bugs,
discussing features, as well as reading and providing feedback on issues
and pull requests, all are valid and necessary ways to help.

Try to stick to best practices:
- SetupTools: https://setuptools.readthedocs.io/en/latest/
- Django: https://docs.djangoproject.com/fr/3.2/intro/reusable-apps/

## Committing Code

The great thing about using a distributed versioning control system like git
is that everyone becomes a committer. When other people write good patches
it makes it very easy to include their fixes/features and give them proper
credit for the work.

We recommend that you do all your work in a separate branch. When you
are ready to work on a bug or a new feature create yourself a new branch. The
reason why this is important is you can commit as often you like. When you are
ready you can merge in the change. Let's take a look at a common workflow:

    git checkout -b 42-task
    ... fix and git commit often ...
    git push origin 42-task

The reason we have created two new branches is to stay off of `master`.
Keeping master clean of only upstream changes makes yours and ours lives
easier. You can then send us a pull request for the fix/feature. Then we can
easily review it and merge it when ready.


### Writing Commit Messages

Writing a good commit message makes it simple for us to identify what your
commit does from a high-level. There are some basic guidelines we'd like to
ask you to follow.

A critical part is that you keep the **first** line as short and sweet
as possible. This line is important because when git shows commits and it has
limited space or a different formatting option is used the first line becomes
all someone might see. If your change isn't something non-trivial or there
reasoning behind the change is not obvious, then please write up an extended
message explaining the fix, your rationale, and anything else relevant for
someone else that might be reviewing the change. Lastly, if there is a
corresponding issue in Github issues for it, use the final line to provide
a message that will link the commit message to the issue and auto-close it
if appropriate.

    Add ability to travel back in time

    You need to be driving 88 miles per hour to generate 1.21 gigawatts of
    power to properly use this feature.

    Fixes #88


## Coding style

We are using the following linters and formaters, please make sure your
editor is set to apply them :

* Black: Python code formater. Installation: `pip install black`. Specific argument: `--line-length=120`
* PyFlakes: unused variables and imports (if your editor do not support it, you can alternatively use Flake8 which is a wrapper with PEP8 support)
* Mypy: type checker using Python annotation.
* isort: Python import sorter.
* bandit: security flaw reporter.

In addition;
* Docstrings always use three double quotes on a line of their own, so, for
  example, a single line docstring should take up three lines not one.


## Pull Requests

Please keep your pull requests focused on one specific thing only. If you
have a number of contributions to make, then please send seperate pull
requests. It is much easier on maintainers to receive small, well defined,
pull requests, than it is to have a single large one that batches up a
lot of unrelated commits.

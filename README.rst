####################
django-smart-testing
####################

Improved Django testing facility (runner, testcase, ...)


Install
=======================================

Add the following code in your test settings:

.. code-block:: python

    TEST_RUNNER = "smart_testing.runners.SmartDiscoverRunner"


Then inherit all your tests from ``smart_testing.testcase.BaseTestCase`` to unlock all features.



To customize your traceback, add the following code in your dev settings:

.. code-block:: python

    sys.excepthook = SmartTraceback(hide_external_files=True, absolute_paths=False, verbose_level=1)


Note that django-smart-testing create files during test execution if you are using assert_num_queries utils. You may add ``.smart_testing`` in your .gitignore to prevent commiting test files.


Features
=======================================

* **Full test name:** when a test fails, the title contains the whole path to allow fast copy/past.
* **Live report:** errors are displayed when occuring (not at the end of the whole suite).
* **Clear output:** logger are captured and displayed only if a test fails. Optionaly, prints can also be captured.
* **Improved readability:** thanks to optional colored outputs.
* **Test helpers:** new usefull context managers (``assertNumQueries``, ``assertNbLogs``), methods (``assertDelta``, ``assertApiEqual``, ``now``, ``today``) and decorators (``print_network``, ``disable_network``, ``track``)
* **Utilities:** lots of small useful functions (enum, canonical urls, file downloader, ...)
* **Multithread helpers:** ``@parallel(nb_threads)`` decorator to easily start multithreaded tests.
* **Speed optimizations compatible:** the suite is compatible with ``--parallel`` and ``--keepdb``, and let you disable migrations without loosing PostgreSql extensions.


Run
=======================================

Tests are run like usual: ``python manage.py test``.

The following standard django test arguments are recommended:

* ``path.to.test``: run a specific sub tests tree.
* ``--parallel``: execute multiple tests at the same time. Note: not compatible with pdb/ipdb.
* ``--keepdb``: do not destroy database between each tests. This improves performances but may change your ids as they are not reset. Database is still cleared between each tests by rolling back transactions.
* ``--noinput``: disable all terminal inputs. Note: not compatible with pdb/ipdb.


Settings
=======================================

Yon can customize this extension by adding a ``SMART_TESTING`` entry in your ``settings.py``.

Example:

.. code-block:: python

    SMART_TESTING = {
        "POSTGRESQL_EXTENSIONS": ["unaccent"],
        "CAPTURE_PRINTS": True,
    }

More infos here: https://gitlab.com/jeremymarc/django-smart-testing/-/blob/master/testing/settings.py


Customize your success ASCII
=======================================

Nothing more rewarding than a nice customized ASCII art to proudly claim your victory!
Make your own symbol of fame and input it in the ``SUCCESS_ASCII_FILEPATH`` settings of ``SMART_TESTING``.

Recommended website: https://www.text-image.com/convert/


Other
=======================================

Interesting projects: https://github.com/revsys/django-test-plus

Todo
=======================================

- Logging helpers to display all queries with a colored formatter when enabling django.db.backends logger

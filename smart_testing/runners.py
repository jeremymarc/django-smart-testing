import logging
import re
import os
import sys
import json
from io import StringIO
from unittest.runner import TextTestResult, TextTestRunner
from unittest.util import strclass

from django.test.runner import DiscoverRunner
from django.utils.functional import cached_property

from colored import stylize
from testfixtures import LogCapture

from smart_testing.cursors import TracebackCursorWrapper
from smart_testing.decorators import disable_network
from smart_testing.settings import testing_settings
from smart_testing.utils import Colors, load_json, titleize, deep_getattr
from smart_testing.exceptions import ResultIgnoredException
from smart_testing.traceback import SmartTraceback

LOG_CAPTURE = None


def get_last_fails_filepath():
    if testing_settings.LAST_FAILS_PATH is None:
        return None
    path = os.path.abspath(testing_settings.LAST_FAILS_PATH)
    if path and not os.path.exists(path):
        os.makedirs(path)
    return os.path.join(path, "last_fails.json")


class PrintCapture:
    """
    Context to capture print outputs on sys.stdout.
    """

    enable = testing_settings.CAPTURE_PRINTS

    def __enter__(self):
        if self.enable:
            testing_settings.log("PrintCapture: start capture")
            self._stdout = sys.stdout
            sys.stdout = StringIO()
        return self

    def __exit__(self, *args):
        if self.enable:
            testing_settings.log("PrintCapture: end capture")
            del sys.stdout  # free up some memory
            sys.stdout = self._stdout


class LiveTextTestResult(TextTestResult):
    """
    This TextTestResult allows display of errors on the fly (not at the end of all tests)
    and print the captured outputs.
    """

    def option(self, name, default=None):
        options = deep_getattr(self, "runner.discoverer.options", {})
        return options.get(name, default)

    def stopTest(self, test):
        global LOG_CAPTURE
        super().stopTest(test)
        if LOG_CAPTURE:
            LOG_CAPTURE.clear()
        del sys.stdout  # free up some memory
        sys.stdout = StringIO()

    def dump_logs(self):
        global LOG_CAPTURE
        if not LOG_CAPTURE or self.option("no_logs", False):
            return
        log_level_name = logging.getLevelName(self.runner.log_level_capture)
        self.stream.writeln(titleize("log %s (--log-level=LEVEL to change, --no-logs to disable)") % log_level_name)
        formatter = logging.Formatter(testing_settings.LOGGING_FORMATER)
        for record in LOG_CAPTURE.records:
            self.stream.writeln(formatter.format(record))

    def dump_prints(self):
        if not PrintCapture.enable:
            return
        records = sys.stdout.getvalue().splitlines()

        if not records:
            return
        self.stream.writeln(titleize("captured prints"))
        # formatter = logging.Formatter(testing_settings.LOGGING_FORMATER)
        for record in records:
            self.stream.writeln(record)

    def _exc_info_to_string(self, err, test):
        kwargs = {
            "hide_external_files": not self.option("show_externals", False),
            "externals_caption": "(Add '--show-externals' to force display)",
        }
        return SmartTraceback(**kwargs).text(*err)

    def addError(self, test, err):
        exc_class, exc, tb = err
        super().addError(test, err)
        self.livePrintError(exc, "ERROR", *self.errors[-1])

    def addFailure(self, test, err):
        exc_class, exc, tb = err
        super().addFailure(test, err)
        self.livePrintError(exc, "FAIL", *self.failures[-1])

    def getDescription(self, test):
        method_name = getattr(test, "_testMethodName", None)
        if not method_name:
            # When error occurs in classmethod, there is no method name
            return re.sub(r"^(.+) \((.+)\)$", r"\2.\1", test.description)
        return "%s.%s" % (strclass(test.__class__), method_name)

    def livePrintError(self, exc, flavour, test, error):
        global LOG_CAPTURE

        if issubclass(exc.__class__, ResultIgnoredException):
            return
        style = stylize if testing_settings.COLORED_OUTPUT else lambda msg, x: msg
        self.stream.writeln()
        self.stream.writeln(self.separator1)
        self.stream.writeln(style("%s: %s" % (flavour, self.getDescription(test)), Colors.error))
        self.stream.writeln(titleize("exception"))
        self.stream.writeln("%s" % error)
        self.dump_logs()
        self.dump_prints()
        self.stream.writeln(self.separator2)
        self.stream.writeln()

    def printErrors(self):
        # No call to super to prevent double print of errors and failures at the end of the test suite
        self.stream.writeln()
        self.update_last_fails()

    def update_last_fails(self):
        filepath = get_last_fails_filepath()
        if not filepath:
            # Feature disabled
            return

        # Listing failed tests
        testing_settings.log("update_last_fails: listing failed tests")
        fails = []
        unicity = set()  # Ensure unicity
        for testcase, err in self.errors + self.failures:
            test_path = self.getDescription(testcase)
            if test_path.startswith("unittest"):
                # In some rare cases, a crash may happen in unittest test functions, we don't want to store that
                continue
            if test_path not in unicity:
                unicity.add(test_path)
                fails.append(test_path)
        # Saving file
        testing_settings.log("update_last_fails: updating failed tests (%s)", filepath)
        try:
            with open(filepath, "w") as fd:
                json.dump(fails, fd)
        except Exception as e:
            testing_settings.log("update_last_fails: update failed (%s)", str(e))
        else:
            testing_settings.log("update_last_fails: update successful")


class PrettyTextTestRunner(TextTestRunner):
    """
    This TextTestRunner captures logs to prevent output spamming.
    Also displays a nice ascii art if your tests are all successful.
    """

    resultclass = LiveTextTestResult

    def _makeResult(self, *args, **kwargs):
        result = super()._makeResult(*args, **kwargs)
        result.runner = self
        return result

    @cached_property
    def logo_success(self):
        testing_settings.log("logo_success: loading %s", testing_settings.SUCCESS_ASCII_FILEPATH)
        return open(testing_settings.SUCCESS_ASCII_FILEPATH, "r").read()

    def option(self, name, default=None):
        options = deep_getattr(self, "discoverer.options", {})
        return options.get(name, default)

    @property
    def log_level_capture(self):
        level = self.option("log_level", logging.ERROR)
        try:
            return logging._checkLevel(level)
        except (ValueError, TypeError):
            testing_settings.log("Invalid logging level: %s", level)
            return logging.ERROR

    @disable_network
    def run(self, suite, *args, **kwargs):
        global LOG_CAPTURE

        testing_settings.log("run: starting test suite (%i tests)", suite.countTestCases())
        if testing_settings.CAPTURE_LOGS:
            testing_settings.log("run: capturing logs")
        with LogCapture(level=self.log_level_capture) as capture:
            LOG_CAPTURE = capture
            testing_settings.log("run: capturing prints")
            with PrintCapture():
                result = super().run(suite, *args, **kwargs)
        LOG_CAPTURE = None
        testing_settings.log("run: test suite completed (%s)", "success" if result.wasSuccessful() else "failure")
        if result.wasSuccessful():
            self.stream.writeln(self.logo_success)
        return result


class SmartDiscoverRunner(DiscoverRunner):
    """
    This DiscoverRunner uses PrettyTextTestRunner and enable PostgreSql extensions.

    - test_suite doc: https://github.com/python/cpython/blob/3.9/Lib/unittest/suite.py
    - parallel_test_suite doc: https://docs.djangoproject.com/fr/2.0/_modules/django/test/runner/
    """

    test_runner = PrettyTextTestRunner

    def __init__(self, *args, **kwargs):
        self.options = {
            "failed": kwargs.pop("failed", False),
            "no_logs": kwargs.pop("no_logs", False),
            "log_level": kwargs.pop("log_level", False),
            "show_externals": kwargs.pop("show_externals", False),
        }
        super().__init__(*args, **kwargs)

    def override_db_cursor(self):
        from django.db.backends import utils as db_utils

        testing_settings.log("override_db_cursor: wrapping db cursor to add traceback")
        db_utils.CursorDebugWrapper = TracebackCursorWrapper

    @classmethod
    def add_arguments(cls, parser):
        # See https://docs.djangoproject.com/en/2.1/_modules/django/test/runner/
        super().add_arguments(parser)
        parser.add_argument("--failed", action="store_true", dest="failed", default=False, help="Run only last failed tests.")
        parser.add_argument("--log-level", action="append", dest="log_level", default=[], help="Select displayed log level.")
        parser.add_argument("--no-logs", action="store_true", dest="no_logs", default=False, help="Do not display captured logs.")
        parser.add_argument(
            "--show-externals", action="store_true", dest="show_externals", default=False, help="Force display of external files in tracebacks."
        )

    @cached_property
    def last_fails(self):
        """
        Return the list of last test failed.
        """
        filepath = get_last_fails_filepath()
        if not filepath:
            # Feature disabled
            return None
        testing_settings.log("last_fails: loading %s", filepath)
        return load_json(filepath, [])

    def run_tests(self, *args, **kwargs):
        style = stylize if testing_settings.COLORED_OUTPUT else lambda msg, x: msg

        self.override_db_cursor()
        if self.parallel == 1:
            # Force disable print capture when running a single test
            PrintCapture.enable = False
        if self.options["failed"]:
            if not self.last_fails:
                print(style("All tests passed the last run !", Colors.green))
                return
            else:
                testing_settings.log("run_tests: adding %i failed tests to the suite", len(self.last_fails))
            # Yes, DiscoverRunner requires a tuple of tuple
            args = (args[0] + tuple(self.last_fails),)
        testing_settings.log("run_tests: starting test suite")
        return super().run_tests(*args, **kwargs)

    def run_suite(self, suite, **kwargs):
        resultclass = self.get_resultclass()
        runner = self.test_runner(verbosity=self.verbosity, failfast=self.failfast, resultclass=resultclass)
        runner.discoverer = self
        return runner.run(suite)

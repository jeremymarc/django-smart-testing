import threading
import traceback

from django.test import TransactionTestCase

from smart_testing.testcase import TestCaseMixin
from smart_testing.exceptions import ParallelException


class MultithreadTestCase(TestCaseMixin, TransactionTestCase):
    """
    Special test class inheriting from 'TransactionTestCase' instead of 'TestCase' to enable real database commit.
    Thoses tests are slower than 'BaseTestCase' because Django needs to clear the database between each test instead
    of doing just a simple rollback.

    Beware: 'TransactionTestCase' does not call 'setUpTestData()', call it manually in each tests its needed.

    Example:
        class MyTestCase(MultithreadTestCase):
            def test_simple_threading(self):
                # Optionaly call parent 'setUpTestData()' method
                self.__class__.setUpTestData()

                # Prepare optional multithread data before execution
                NB_THREADS = 5
                barrier = threading.Barrier(NB_THREADS)  # Usefull for synchronisation
                results = dict()

                # Define multithreaded function
                @parallel(NB_THREADS)
                def execute(thread_id):
                    results[thread_id] = some_computations()

                # Execute parallel task
                execute()
                # Optional post execution checks
                self.assertLen(results, NB_THREADS)
    """

    pass


def parallel(nb_threads, fail_on_error=True):
    """
    Add this decorator to small pieces of code that you want to test
    concurrently to make sure they don't raise exceptions when run at the
    same time.

    Calling the decorated function will start the required number of threads
    and then wait for them to finish.

    Tips: some Django views that do a SELECT and then a subsequent
    INSERT might fail when the INSERT assumes that the data has not changed
    since the SELECT. This is useful if you want to test toughness upon
    concurrent operations (like double payment or double signin).

    See 'MultithreadTestCase' for example.
    """

    def parallel_decorator(func):
        """
        Final decorator which will be applied to the function with nb_threads closure.
        """

        def wrapper(*args, **kwargs):
            """
            Wrapper starting the function 'nb_threads' times.
            """
            exceptions = {}
            threads = []
            # Initializing threads
            for i in range(nb_threads):

                def threaded_func(thread_id=i):
                    """
                    Wrapper around the function executed in each thread.
                    """
                    try:
                        func(thread_id, *args, **kwargs)
                    except Exception as e:
                        exceptions[thread_id] = e
                    finally:
                        """
                        In django, each thread has its own database connection.
                        We must manually close them to prevent a warning during tests execution.
                        """
                        from django.db import connection

                        connection.close()

                threads.append(threading.Thread(target=threaded_func))
            # Starting threads
            for t in threads:
                t.start()
            # Waiting threads
            for t in threads:
                t.join()
            # Handling exceptions
            if exceptions and fail_on_error:
                raise ParallelException("%s exception(s) catched during parallel execution: %s" % (len(exceptions), exceptions))
            return exceptions

        return wrapper

    return parallel_decorator


def format_parallel_exceptions(exceptions):
    res = ""
    pattern = "#" * 30
    for thread_id, e in exceptions.items():
        res += f"\n{pattern} thread_id {thread_id} {pattern}\n\n"
        res += "".join(traceback.format_exception(e.__class__, e, e.__traceback__))
    return res

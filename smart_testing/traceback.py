import logging
import os
import sys
import types
import token
import tokenize

logger = logging.getLogger(__name__)


"""
This utility is formatting the exception traceback to be more compact and readable.

The main features are:
- Output colorization
- Hidding of external sources with 'hide_external_files' (default=True)
- Display of relative paths with 'absolute_paths' (default=False)
- Verbosity level from 0 to 3 adding contextual informations (default=1)
  0. Displays the exception message
  1. Displays the traceback with 1 contextual line per file
  2. Displays the traceback with 5 contextual line per file
  3. Displays the traceback with 5 contextual line per file and all parameter values
- Can start a debugging session where the exception occurs with 'call_pdb' (default=False)
- Can specify the relative path root used to display filepath with 'root' (default=os.getcwd())

Usage:
    sys.excepthook = SmartTraceback(hide_external_files=True, absolute_paths=False, verbose_level=1, call_pdb=False, root=None)
"""


def generate_traceback(start_depth=None):
    """
    Generate a python traceback object.

    Inpired from: https://stackoverflow.com/questions/27138440/how-to-create-a-traceback-object
    """
    tb = None
    depth = start_depth or 0
    while True:
        try:
            frame = sys._getframe(depth)
            depth += 1
        except ValueError as exc:
            break
        tb = types.TracebackType(tb, frame, frame.f_lasti, frame.f_lineno)
    return tb


try:
    from IPython.core import ultratb
    from IPython.utils import PyColorize
    from IPython.utils.coloransi import ColorScheme, InputTermColors, TermColors

    DEFAULT_COLOR_SCHEME = ColorScheme(
        "default",
        {
            # The color to be used for the top line
            "topline": TermColors.Brown,
            # The colors to be used in the traceback
            "filename": TermColors.Cyan,
            "lineno": TermColors.LightBlue,
            "name": TermColors.Cyan,
            "vName": TermColors.Purple,
            "val": TermColors.Purple,
            "em": TermColors.LightBlue,
            # Emphasized colors for the last frame of the traceback
            "normalEm": TermColors.LightGray,
            "filenameEm": TermColors.LightCyan,
            "linenoEm": TermColors.LightBlue,
            "nameEm": TermColors.LightCyan,
            "valEm": TermColors.LightPurple,
            # Colors for printing the exception
            "excName": TermColors.LightRed,
            "line": TermColors.LightGray,
            "caret": TermColors.White,
            "Normal": TermColors.Normal,
            # Other
            "header": TermColors.LightRed,
            token.NUMBER: TermColors.LightCyan,
            token.OP: TermColors.Yellow,
            token.STRING: TermColors.LightBlue,
            tokenize.COMMENT: TermColors.LightRed,
            token.NAME: TermColors.Normal,
            token.ERRORTOKEN: TermColors.Red,
            PyColorize._KEYWORD: TermColors.LightGreen,
            PyColorize._TEXT: TermColors.Yellow,
            "in_prompt": InputTermColors.Green,
            "in_number": InputTermColors.LightGreen,
            "in_prompt2": InputTermColors.Green,
            "in_normal": InputTermColors.Normal,  # color off (usu. Colors.Normal)
            "out_prompt": TermColors.Red,
            "out_number": TermColors.LightRed,
            "normal": TermColors.Normal,  # color off (usu. Colors.Normal)
        },
    )
    PyColorize.ANSICodeColors.add_scheme(DEFAULT_COLOR_SCHEME)

    class SmartTraceback(ultratb.FormattedTB):
        def __init__(self, hide_external_files=True, absolute_paths=False, verbose_level=1, root=None, externals_caption=None, **kwargs):
            self.hide_external_files = hide_external_files
            self.absolute_paths = absolute_paths
            self.externals_caption = (
                externals_caption if externals_caption is not None else "(set 'SmartTraceback.hide_external_files' to False to force display)"
            )
            self.root = (root or os.getcwd()) + os.path.sep
            defaults = {
                "long_header": True,
                "mode": ("Minimal", "Plain", "Context", "Verbose")[verbose_level],
            }
            defaults.update(kwargs)
            super().__init__(**defaults)
            self.color_scheme_table.add_scheme(DEFAULT_COLOR_SCHEME)
            self.set_colors("default")

        def _format_filename(self, filename):
            if not self.absolute_paths:
                filename = filename.replace(self.root, "")
            return filename

        def _format_list(self, extracted_list):
            C = self.Colors
            list = []
            external_files = []
            for filename, lineno, name, line in extracted_list[:-1]:
                if self.hide_external_files:
                    if self.root not in filename and filename[0] == os.path.sep:
                        external_files.append(filename)
                        continue
                    if len(external_files):
                        item = f"  {C.valEm}{len(external_files)}{C.val} external files skipped {self.externals_caption}\n{C.Normal}"
                        list.append(item)
                        external_files = []
                filename = self._format_filename(filename)
                item = f"{C.Normal}  {C.filename}{filename}{C.Normal} @ {C.lineno}{lineno}{C.Normal} => {C.name}{name}{C.Normal}\n"
                if line:
                    item += "    %s\n" % line.strip()
                list.append(item)
            if len(external_files):
                item = f"  {C.valEm}{len(external_files)}{C.val} external files skipped {self.externals_caption}\n{C.Normal}"
                list.append(item)
            # Emphasize the last entry
            filename, lineno, name, line = extracted_list[-1]
            filename = self._format_filename(filename)
            item = f"{C.normalEm}  {C.filenameEm}{filename}{C.normalEm} @ {C.linenoEm}{lineno}{C.normalEm} => {C.nameEm}{name}{C.Normal}\n"
            if line:
                item += "%s    %s%s\n\n" % (C.line, line.strip(), C.Normal)
            list.append(item)
            return list

except ImportError:

    def SmartTraceback(*args, **kwargs):
        logger.info("IPython import failed, falling back to default exception handler")
        return sys.excepthook

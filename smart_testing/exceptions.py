class ResultIgnoredException(Exception):
    """
    All exceptions inheriting from this class won't be displayed by the result class.

    This is useful for exceptions which have already been displayed one (like ResultIgnoredException).
    """

    pass


class MonitorNetworkException(Exception):
    pass


class DisableNetworkException(ResultIgnoredException):
    pass


class DownloadException(Exception):
    pass


class ParallelException(Exception):
    pass


class AssertNumQueriesException(Exception):
    pass

import logging
import re
import os
import json
import sys
import traceback
from contextlib import contextmanager
from datetime import timedelta

from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.utils import timezone

import colored
import magicattr
import requests

from smart_testing.settings import testing_settings
from smart_testing.exceptions import DownloadException

logger = logging.getLogger(__name__)


def enum(**kwargs):
    class Enum(tuple):
        def __contains__(self, item):
            for k, v in self:
                if k == item:
                    return True
            return False

        def get(self, item, default=None):
            for k, v in self:
                if k == item:
                    return k
            return default

        def keys(self):
            return tuple(k for k, v in self)

    res = Enum((k, v) for k, v in kwargs.items())
    for i, (k, v) in enumerate(res):
        setattr(res, k, k)
        setattr(res, "%s_display" % k, v)
    return res


def deep_getattr(obj, attrs, default=None):
    try:
        return magicattr.get(obj, attrs)
    except AttributeError:
        return default


def partition(iterable, predicate):
    true_items = []
    false_items = []
    for item in iterable:
        if predicate(item):
            true_items.append(item)
        else:
            false_items.append(item)
    return true_items, false_items


def path_join(*args, trailing_slash=False):
    root = args[0]
    if root and root[-1] != "/":
        root += "/"
    path = root + "/".join(a.strip("/") for a in args[1:] if a.strip("/"))
    if path and path[-1] != "/" and trailing_slash:
        return path + "/"
    return path


def flush(text):
    sys.stdout.write(text)
    sys.stdout.flush()


def progress(text, num, step, color=None):
    if not num % step:
        flush(colored.stylize(text, color) if color else text)


class Colors:
    # Basic colors
    red = colored.fg("red")
    red_light = colored.fg("light_red")
    red_dark = colored.fg("dark_red_1")

    green = colored.fg("green")
    green_light = colored.fg("light_green")
    green_dark = colored.fg("dark_green")

    blue = colored.fg("blue")
    cyan = colored.fg("cyan")
    yellow = colored.fg("yellow")
    purple = violet = colored.fg("violet")

    gray_light = colored.fg("light_gray")
    gray_dark = colored.fg("dark_gray")

    # Text
    bold = colored.attr("bold")
    dim = colored.attr("dim")

    # Generics
    error = bold + red
    warning = bold + yellow
    success = bold + green
    info = bold + blue


def titleize(title, separator="-", width=70):
    title = f" {title.strip()} "
    separator_width = width - len(title)
    nb_left_separators = (separator_width // 2) // len(separator)
    nb_right_separators = (separator_width - nb_left_separators * len(separator)) // len(separator)
    return separator * nb_left_separators + title + separator * nb_right_separators


def canonical_url(path, request=None, domain=None, scheme=None):
    from django.contrib.sites.models import Site

    if domain is None:
        if request:
            domain = request.get_host()
        else:
            domain = Site.objects.first().domain
    if scheme is None:
        scheme = "https" if request and request.is_secure() else "http"
    return path_join(f"{scheme}://{domain}", path)


def format_price(price):
    """Serialize float price to string as if it was a decimal"""
    return "%.2f" % price if price is not None else None


@contextmanager
def download_file(url, timeout=None):
    response = requests.get(url, timeout=timeout, stream=True)
    if not response.ok:
        raise DownloadException("DOWNLOAD_FAILED", details={"url": url, "response": response})
    with NamedTemporaryFile() as tmp:
        for chunk in response.iter_content(chunk_size=1024 * 8):
            if not chunk:
                break
            tmp.write(chunk)

        tmp.seek(0)
        yield File(tmp)


@contextmanager
def warn_if_last_more_than(log_level=logging.WARNING, **kwargs):
    now = timezone.now()
    timeout = timedelta(**kwargs)
    yield
    time_elapsed = timezone.now() - now
    if time_elapsed > timeout:
        logger.log(log_level, "Function took too long: %s > %s", time_elapsed, timeout)


def ignore_stack_line(path, line_number):
    for pattern in testing_settings.IGNORED_PATH_STACK_PATTERNS:
        if re.match(pattern, path):
            return True
    return False


def get_smart_last_traceback(prompt="> "):
    stack = traceback.extract_stack()
    for path, line_number, _, _ in reversed(stack):
        if ignore_stack_line(path, line_number):
            continue
        return f"{prompt}{path}:{line_number}"
    return f"{prompt}TRACEBACK NOT FOUND"


def load_json(filepath, default):
    path, filename = os.path.split(filepath)
    if path and not os.path.exists(path):
        # Create path
        os.makedirs(path)
    if not os.path.isfile(filepath):
        # Reset data
        with open(filepath, "w") as fd:
            fd.write(json.dumps(default))
        return default
    # Load data
    try:
        with open(filepath, "r") as fd:
            return json.load(fd)
    except Exception as e:
        logger.error(
            f"Crash during file {filepath!r} loading. This can happen if the test process is killed during file generation. Try to delete it."
        )
        raise

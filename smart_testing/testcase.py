import datetime
from contextlib import contextmanager
from decimal import Decimal

from django.contrib.sites.models import Site
from django.db.models.query import QuerySet
from django.test import RequestFactory, TestCase as DjangoTestCase
from django.urls import reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.contrib.admin.sites import AdminSite

from smart_testing.contexts import AssertNbLogsContext, MonitorQueriesContext
from smart_testing.settings import testing_settings
from smart_testing.utils import format_price


class TestCaseMixin:
    maxDiff = testing_settings.MAX_DIFF

    @classmethod
    def now(cls):
        return timezone.now()

    @classmethod
    def today(cls):
        return cls.now().date()

    @classmethod
    def _install_pgsql_extensions(cls):
        from django.db import connection

        with connection.cursor() as cursor:
            for extension in testing_settings.POSTGRESQL_EXTENSIONS:
                cursor.execute(f"CREATE EXTENSION IF NOT EXISTS {extension};")

    @classmethod
    def setUpClass(cls):
        cls._install_pgsql_extensions()
        super().setUpClass()
        cls.site = Site.objects.get()
        cls.site.domain = testing_settings.SERVER_DOMAIN
        cls.site.name = testing_settings.SERVER_NAME
        cls.site.save()

    def tearDown(self):
        super().tearDown()
        self.clear_queries_cache()

    @method_decorator(contextmanager)
    def assertDelta(self, count, delta=1):
        old_count = count()
        yield
        new_count = count()
        self.assertEqual(old_count + delta, new_count)

    def assertLen(self, container, expected_length: int):
        if isinstance(container, QuerySet):
            container_length = container.count()
        else:
            container_length = len(container)
        self.assertEqual(container_length, expected_length)

    @classmethod
    def clear_queries_cache(cls):
        """
        Force cache clear to prevent sporadic assertNumQueries failures.
        """
        Site.objects.clear_cache()

    def assertNumQueries(self, expected_num_queries: int):
        """
        New implementation of TransactionTestCase.assertNumQueries (better output, diff feature)
        --> See documentation in MonitorQueriesContext.
        """
        return MonitorQueriesContext(
            expected_num_queries,
            test_case=self,
            output_format=testing_settings.ASSERT_NUM_QUERIES_FORMAT,
            raise_error=testing_settings.ASSERT_NUM_QUERIES_INTERRUPT_TEST,
        )

    def assertStartsWith(self, string: str, starts_with: str):
        if not string.startswith(starts_with):
            raise AssertionError(f"string '{string}' does not starts with '{starts_with}'")

    def _apiAssertionError(self, context, message):
        return AssertionError("### API result != expected ### Context: data%s\n%s" % (context, message))

    def assertApiEqual(self, result, expected, pagination=None, context=None, **kwargs):
        if context is None:
            context = ""
        if pagination:
            count, next_page, previous_page = pagination
            expected = {"count": count, "next_page": next_page, "previous_page": previous_page, "results": expected}

        try:
            if issubclass(type(expected), dict):
                return self.assertApiDictEqual(result, expected, context, **kwargs)
            elif issubclass(type(expected), list):
                return self.assertApiListEqual(result, expected, context, **kwargs)
        except AssertionError as e:
            # This try/except only prevent the recursive traceback
            raise e
        try:
            self.assertApiValueEqual(result, expected)
        except AssertionError as e:
            # Changing error message to display context
            raise self._apiAssertionError(context, e) from None

    def assertApiValueEqual(self, result, expected):
        if isinstance(expected, float):
            assert type(result) != Decimal, "Can't compare Decimal and Float. Please use an expected String or " "Decimal value."
            self.assertAlmostEqual(result, expected)
            return
        if type(expected) == type(result):
            self.assertEqual(result, expected)
            return
        converters = {
            datetime.datetime: lambda v: v.strftime(testing_settings.API_DATETIME_FORMAT),
            datetime.date: lambda v: v.strftime(testing_settings.API_DATE_FORMAT),
            Decimal: format_price,
        }
        expected = converters.get(type(expected), lambda x: x)(expected)
        result = converters.get(type(result), lambda x: x)(result)
        self.assertEqual(result, expected)

    def assertApiDictEqual(self, result, expected, context=None, ignoreExtras=False, **kwargs):
        if context is None:
            context = ""
        if not issubclass(type(expected), dict) or not issubclass(type(result), dict):
            msg = "Expected (dict, dict) found " + str((type(expected).__name__, type(result).__name__))
            raise self._apiAssertionError(context, msg)
        # Missing keys
        missings = set(expected) - set(result)
        if missings:
            msg = "Missing keys: " + ", ".join("%s (%s)" % (m, expected[m]) for m in missings)
            raise self._apiAssertionError(context, msg)
        # Special empty case
        if result and not expected:
            msg = "Expecting empty dict, found %s" % str(result)
            raise self._apiAssertionError(context, msg)
        # Extra keys
        extras = set(result) - set(expected)
        if extras and not ignoreExtras:
            msg = "Additional keys found (use ignoreExtras=True to dismiss): " + ", ".join("%s (%s)" % (e, result[e]) for e in extras)
            raise self._apiAssertionError(context, msg)
        # Compare keys
        for k in expected.keys():
            sub_msg = "%s[%s]" % (context, repr(k))
            self.assertApiEqual(result[k], expected[k], context=sub_msg, ignoreExtras=ignoreExtras, **kwargs)

    def assertApiListEqual(self, result, expected, context=None, **kwargs):
        if context is None:
            context = ""
        if not issubclass(type(expected), list) or not issubclass(type(result), list):
            msg = "Expected (list, list) found " + str((type(expected).__name__, type(result).__name__))
            raise self._apiAssertionError(context, msg)
        if len(expected) != len(result):
            msg = "Expected %i items, %i found" % (len(expected), len(result))
            raise self._apiAssertionError(context, msg)
        for i, (r, e) in enumerate(zip(result, expected)):
            sub_msg = "%s[%i]" % (context, i)
            self.assertApiEqual(r, e, context=sub_msg, **kwargs)

    def assertNbLogs(self, expected_count, logger=None, level=None, context=None):
        return AssertNbLogsContext(self, expected_count, logger, level, context=context)


class BaseTestCase(TestCaseMixin, DjangoTestCase):
    pass


class AdminTestCase(BaseTestCase):
    ADMIN = None
    FACTORY_CALLBACK_NAME = None
    NB_OBJECTS_CREATED = 10
    NB_OBJECTS_DELETED = 1
    NB_EXISTING_OBJECTS = 0

    @property
    def model(self):
        return self.ADMIN.model

    @property
    def db_table(self):
        return self.model._meta.db_table

    @classmethod
    def camel_to_snake(cls, name):
        """
        This trick allow to automate the call to factory methods `make_<classname>()` based only on class name.
        Please, keep the naming convention going!
        """
        name = re.sub("(.)([A-Z][a-z]+)", r"\1_\2", name)
        return re.sub("([a-z0-9])([A-Z])", r"\1_\2", name).lower()

    @classmethod
    def make_object(cls, index):
        raise NotImplementedError()

    @classmethod
    def make_objects(cls):
        if cls.NB_OBJECTS_CREATED:
            return [cls.make_object(i) for i in range(cls.NB_OBJECTS_CREATED)]
        return []

    @classmethod
    def delete_object(cls, object):
        object.delete()

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.factory = RequestFactory()
        cls.NB_EXISTING_OBJECTS = cls.ADMIN.model.objects.count()
        if cls.NB_OBJECTS_CREATED:
            callback_name = cls.FACTORY_CALLBACK_NAME or "make_%s" % cls.camel_to_snake(cls.ADMIN.model._meta.object_name)
            creation_callback = getattr(cls, callback_name)
            cls.new_objects = [creation_callback() for i in range(cls.NB_OBJECTS_CREATED)]

    def assert_access_restricted(self, url, user):
        redirect_url = reverse("admin:login") + f"?next={url}"
        request = self.factory.get(url)
        request.user = user
        self.assertRedirects(self.ADMIN.add_view(request), redirect_url)
        self.assertRedirects(self.ADMIN.delete_view(request), redirect_url)
        self.assertRedirects(self.ADMIN.change_view(request), redirect_url)
        self.assertRedirects(self.ADMIN.changelist_view(request), redirect_url)

    def assert_list(self, nb_queries, user, *args):
        nb_expected_results = self.NB_OBJECTS_CREATED + self.NB_EXISTING_OBJECTS
        self.assertGreater(self.model.objects.count(), 2, "You should add more objects to detect N+1 queries")
        url = self.list_url(*args)
        # self.assert_access_restricted(url)
        request = self.factory.get(url)
        request.user = user
        with self.assertNumQueries(nb_queries):
            response = self.ADMIN.changelist_view(request)
            self.assertEqual(response.context["cl"].result_count, nb_expected_results)
            self.assertEqual(response.status_code, 200)
        # Assert number of queries doesn't depend on number of objects
        if self.NB_OBJECTS_DELETED:
            for i in range(self.NB_OBJECTS_DELETED):
                object = self.new_objects.pop()
                self.delete_object(object)
            with self.assertNumQueries(nb_queries):
                response = self.ADMIN.changelist_view(request)
                self.assertEqual(response.context["cl"].result_count, nb_expected_results - self.NB_OBJECTS_DELETED)

    def assert_delete(self, nb_queries, user, data, *args):
        url = self.detail_url(*args)
        request = self.factory.post(url, data=data)
        request.user = user
        with self.assertNumQueries(nb_queries):
            response = self.ADMIN.delete_view(request)
            self.assertEqual(response.status_code, 204)
        return response

    def assert_get(self, nb_queries, user, *args):
        url = self.detail_url(*args)
        request = self.factory.get(url)
        request.user = user
        with self.assertNumQueries(nb_queries):
            response = self.ADMIN.change_view(request)
            self.assertEqual(response.status_code, 200)
        return response

    def assert_search(self, nb_queries, search_term, expected_results, user):
        instance = self.ADMIN(model=self.model, admin_site=AdminSite())
        request = self.factory.get(self.list_url())
        request.user = user
        with self.assertNumQueries(nb_queries):
            results, use_distinct = instance.get_search_results(request, self.model.objects.all(), search_term=search_term)
        self.assertEqual(list(results.all()), expected_results)

    def detail_url(self, *args, **kwargs):
        base_url = "admin:%s_change" % self.db_table
        return reverse(base_url, args=args, **kwargs)

    def list_url(self, *args, **kwargs):
        base_url = "admin:%s_changelist" % self.db_table
        return reverse(base_url, args=args, **kwargs)

import logging
from time import time

import django.db.backends.utils as db_utils

from smart_testing.utils import get_smart_last_traceback


class TracebackCursorWrapper(db_utils.CursorWrapper):
    """
    Reimplementation of Django's CursorDebugWrapper that also stores
    traceback in order to be used in MonitorQueriesContext.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Using the same logger as django's to ensure queries are displayed if user want's it
        self.django_logger = logging.getLogger("django.db.backends")

    def get_traceback(self):
        return get_smart_last_traceback()

    def execute(self, sql, params=None):
        start = time()
        try:
            return super().execute(sql, params)
        finally:
            stop = time()
            duration = stop - start
            sql = self.db.ops.last_executed_query(self.cursor, sql, params)
            self.db.queries_log.append({"sql": sql, "time": "%.3f" % duration, "traceback": self.get_traceback()})
            self.django_logger.debug("(%.3f) %s; args=%s", duration, sql, params, extra={"duration": duration, "sql": sql, "params": params})

    def executemany(self, sql, param_list):
        start = time()
        try:
            return super().executemany(sql, param_list)
        finally:
            stop = time()
            duration = stop - start
            try:
                times = len(param_list)
            except TypeError:
                times = "?"
            self.db.queries_log.append({"sql": "%s times: %s" % (times, sql), "time": "%.3f" % duration, "traceback": self.get_traceback()})
            self.django_logger.debug("(%.3f) %s; args=%s", duration, sql, param_list, extra={"duration": duration, "sql": sql, "params": param_list})

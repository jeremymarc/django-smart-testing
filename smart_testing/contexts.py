import inspect
import json
import logging
import os
import re
import sys
from contextlib import ContextDecorator, ExitStack
from unittest import mock
from difflib import SequenceMatcher
from functools import cached_property

from django.utils import timezone

from smart_testing.traceback import generate_traceback

try:
    # Compatibility: Python <= 3.8
    from unittest.case import _AssertLogsContext
except ImportError:
    # Compatibility: Python >= 3.9
    from unittest._log import _AssertLogsContext

from django import VERSION as DJANGO_VERSION
from django.db import DEFAULT_DB_ALIAS, connections
from django.test.utils import CaptureQueriesContext

from colored import stylize

from smart_testing.settings import testing_settings
from smart_testing.utils import Colors, load_json, get_smart_last_traceback
from smart_testing.exceptions import DisableNetworkException, MonitorNetworkException, AssertNumQueriesException

style = stylize if testing_settings.COLORED_OUTPUT else lambda msg, x: msg
logger = logging.getLogger(__name__)


def update_testcase_outcome_error(test_case, exc_info):
    outcome = test_case._outcome
    outcome.success = False
    if DJANGO_VERSION < (4, 0):
        # Compatibility: Django <4
        outcome.errors.append((test_case, exc_info))
    elif DJANGO_VERSION < (5, 0):
        # Compatibility: Django <5
        outcome.result.errors.append((test_case, exc_info))
    else:
        # Compatibility: Django >=5
        outcome.result.addError(test_case, exc_info)


class MonitorNetworkContext(ContextDecorator, ExitStack):
    def __init__(self, network_functions=None, test_case=None):
        """
        Generate mock.patch on all network functions to prevent any external call in a given context.

        Usage as a context manager:
            class SomeTest(TestCase):
                def run(self)
                    with MonitorNetworkContext(test_case=self):
                        return super().run()

        Usage as decorator:
            @MonitorNetworkContext():
            def send(...):
                # Network code

        Note:
            During test execution, the exception may appear 2 times:
            - First time when the call is made and the error added in the test case
            - Second time when catched by the test if the exception risen hasn't been catched

            But this is necessary so that the error is still visible if it is catched by the test.
            We want to forbid any external call which has not been mocked during tests run, catched or not.
        """
        super().__init__()
        network_functions = network_functions if network_functions is not None else testing_settings.DISABLE_NETWORK_FUNCTIONS
        self.test_case = test_case
        self.cm_list = []
        for _func_path in network_functions:
            # Retrieving original function
            _module_getter, _func_name = mock._get_target(_func_path)
            _original = getattr(_module_getter(), _func_name)

            class Wrapper:
                """
                This wrapper prevents closure and manually add the error in the test results.
                """

                # Prevents closure issues
                func_path = _func_path
                original = staticmethod(_original)

                @classmethod
                def __call__(this, *args, **kwargs):
                    if self.allow_call(this.func_path, args, kwargs):
                        return this.original(*args, **kwargs)
                    # Generate detailed message
                    msg = self.format_message(this.func_path, *args, **kwargs)
                    # Generate exception
                    if self.test_case:
                        # Add error in test case
                        self.add_test_case_error(msg)
                    # Stop execution
                    raise DisableNetworkException(msg)

            self.cm_list.append(mock.patch(_func_path, Wrapper()))

    def allow_call(self, caller, args, kwargs):
        """
        Overload this function if you want to let specific calls pass through.
        You can modify args and kwargs before the call to the original function.
        """
        return False

    def add_test_case_error(self, msg):
        tb = generate_traceback(3)  # generate_traceback, add_error and wrapper frames are ignored
        msg += style(
            "\n\nWarning: MonitorNetworkException can appear a second time as a DisableNetworkException. If so, you can ignore the DisableNetworkException.",
            Colors.yellow,
        )
        exc_info = (MonitorNetworkException, MonitorNetworkException(msg).with_traceback(tb), tb)
        update_testcase_outcome_error(self.test_case, exc_info)
        # Explicitly break a reference cycle: exc_info -> frame -> exc_info
        exc_info = None

    def customize_caller_data(self, args, **kwargs):
        """
        Overload this method to handle more custom classes
        """
        from requests.models import PreparedRequest
        from urllib.request import Request

        kwargs["args"] = []
        for arg in args:
            if isinstance(arg, PreparedRequest):
                kwargs["url"] = f"{arg.method} {arg.url}"
                kwargs["headers"] = arg.headers
            elif isinstance(arg, Request):
                kwargs["url"] = f"{arg.get_method()} {arg.get_full_url()}"
                kwargs["headers"] = arg.headers
            else:
                kwargs["args"].append(arg)
        return kwargs

    def format_message(self, caller, *args, **kwargs):
        bullet_point = " - "
        style = stylize if testing_settings.COLORED_OUTPUT else lambda msg, x: msg  # TODO: should use common.utils.style
        bold = lambda s: style(str(s), Colors.bold)
        details = {
            "url": "<unknown url>",
            "origin": get_smart_last_traceback(prompt=""),
            "caller": caller,
            "args": args,
            "kwargs": kwargs,
            "headers": "<unknown headers>",
        }
        details = self.customize_caller_data(**details)
        url = style(bold(details.get("url", "")), Colors.cyan)
        msg = f"{bold(caller)}\n\n{url}\n{bullet_point}" + f"\n{bullet_point}".join(f"{k}: {bold(v)}" for k, v in details.items())
        return msg

    def __enter__(self):
        """
        Applying all mock.patch on all functions.
        """
        result = super().__enter__()
        for cm in self.cm_list:
            self.enter_context(cm)
        return result


class MonitorQueriesContext(CaptureQueriesContext):
    """
    New context with:
    - Additionnal output configuration (colors, origin, ...)

    Usage:
        with MonitorQueriesContext(42):
            # Some code
    """

    def __init__(self, nb_expected_queries, test_case=None, connection=None, output_format="FULL", raise_error=True, reference_path=None):
        self.reference_path = reference_path if reference_path else testing_settings.ASSERT_NUM_QUERIES_REFERENCE_PATH
        self.test_case = test_case
        self.nb_expected_queries = nb_expected_queries
        self.output_format = output_format
        self.raise_error = raise_error
        self.assert_num_queries_index = None  # Index of the assert in current test method
        connection = connection or connections[DEFAULT_DB_ALIAS]
        super().__init__(connection)

    @cached_property
    def batch_id(self):
        """
        Returns a unique reference to a query_batch
        """
        if self.test_case is None:
            # Non test context
            return f"CODE:{self.parent_frame.filename}@{self.parent_frame.lineno}"
        # Tests are running
        cls = self.test_case.__class__
        ref = f"TEST:{cls.__module__}.{cls.__name__}.{self.test_case._testMethodName}#{self.assert_num_queries_index}"
        if self.test_case._subtest:
            ref += f"@{self.test_case._subtest.params['name']}"
        return ref

    @cached_property
    def reference_filepath(self):
        if not self.reference_path:
            return None
        path = os.path.abspath(self.reference_path)
        cls = self.test_case.__class__
        filename = f"{cls.__module__}.{cls.__name__}.json"
        return os.path.join(path, filename)

    @cached_property
    def reference(self):
        """
        Return the dict containing last run test infos.

        Warning: there is 1 file per testcase classes to allow tests run with --parallel to work.
        """
        if not self.reference_filepath:
            # Smart testing query count disabled
            return None
        return load_json(self.reference_filepath, dict())

    def update_reference(self):
        if not testing_settings.ASSERT_NUM_QUERIES_REFERENCE_ENABLED or self.reference is None:
            # Smart testing query count disabled
            return
        self.reference[self.batch_id] = {
            "filename": self.parent_frame.filename,
            "lineno": self.parent_frame.lineno,
            "function": self.parent_frame.function,
            "code": self.parent_frame.code_context,
            "nb_expected_queries": self.nb_expected_queries,
            "queries": [{"sql": q["sql"], "traceback": q.get("traceback", "???"), "time": q["time"]} for q in self.captured_queries],
            "id": self.batch_id,
            "reference_date": str(timezone.now()),
        }
        with open(self.reference_filepath, "w") as fd:
            json.dump(self.reference, fd)

    def format_context(self, context, index=None, prompt=None, colored=None):
        # With contextual information
        diff_colors = {"=": Colors.bold, "+": Colors.red, "-": Colors.green}
        tb = context.get("traceback", "???")
        time_value = float(context["time"])
        time = " (%s)" % context["time"]
        index = "" if index is None else f"{index+1:3} "
        if prompt is not None:
            # Removing existing prompt
            tb = tb[2:]
            prompt_color = diff_colors.get(prompt[0], Colors.gray_dark)
        else:
            prompt = ""
            prompt_color = Colors.gray_dark
        add_colors = testing_settings.COLORED_OUTPUT if colored is None else colored
        if add_colors:
            tb = style(tb, Colors.gray_dark)
            index = style(index, prompt_color)
            prompt = style(prompt, prompt_color)
            if time_value < testing_settings.ASSERT_NUM_QUERIES_TIME_WARNING_THRESHOLD:
                time_color = Colors.green
            elif time_value < testing_settings.ASSERT_NUM_QUERIES_TIME_ERROR_THRESHOLD:
                time_color = Colors.yellow
            else:
                time_color = Colors.red_light
            time = style(time, time_color)
        return index + prompt + tb + time

    def format_query(self, sql, raw=None, hide_variables=False, colored=None):
        """
        Format a single SQL query according to settings (raw, or with colors and spacers for human reading only).
        """
        raw = raw if raw is not None else bool("RAW" in self.output_format)
        if raw:
            # No transformation
            return sql

        # With transformation
        sql = re.sub(r"^SELECT \".* FROM", "SELECT", sql)
        sql = re.sub(r"VALUES \".* (ON|RETURNING)", r"VALUES \1", sql)
        sql = re.sub(r"SET \".* WHERE", "WHERE", sql)
        sql = re.sub(r"JOIN \"([^ ]+)\" ON \([^)]+\)", r"JOIN \1", sql)
        sql = re.sub(r"IN \([^)]+\)", r"IN (...)", sql)
        sql = re.sub(r'"', "", sql)

        if hide_variables:
            sql = re.sub(r"'[^']*'", "'...'", sql)
            sql = re.sub(r"SAVEPOINT [_a-zA-Z0-9]+", "SAVEPOINT <id>", sql)
            sql = re.sub(r"[ ,(]\d+", " <num>", sql)
            sql = re.sub(r"DECLARE [_a-zA-Z0-9]+", "DECLARE <id>", sql)

        if testing_settings.COLORED_OUTPUT if colored is None else colored:
            # With colors
            sql = re.sub(r"\b(SELECT|FROM|GROUP BY|ORDER BY|ASC|DESC|LIMIT)\b", style(r"\1", Colors.green + Colors.bold), sql)
            sql = re.sub(r"\b(UPDATE|SET|INSERT|INTO|VALUES)\b", style(r"\1", Colors.yellow + Colors.bold), sql)
            sql = re.sub(r"\b(DELETE)\b", style(r"\1", Colors.red + Colors.bold), sql)
            sql = re.sub(r"\b(WHERE|OR|AND|NOT|IN)\b", style(r"\1", Colors.violet), sql)
            sql = re.sub(r"\b(LEFT.*?JOIN|RIGHT.*?JOIN|INNER JOIN|ON)\b", style(r"\1", Colors.blue + Colors.bold), sql)

        return sql

    def compute_diff(self, queries_ref, queries_result):
        output = []
        # Helpers
        get_context_exp = lambda i, prompt: self.format_context(queries_ref[i], index=i, prompt=prompt)
        get_context_exec = lambda j, prompt: self.format_context(queries_result[j], index=j, prompt=prompt)
        get_sql_exp = lambda i: self.format_query(queries_ref[i]["sql"])
        get_sql_exec = lambda j: self.format_query(queries_result[j]["sql"])
        # Computing specific sql formatting for comparison
        executed_queries = [self.format_query(q["sql"], hide_variables=True, colored=False) for q in queries_result]
        reference_queries = [self.format_query(q["sql"], hide_variables=True, colored=False) for q in queries_ref]
        # Parsing matched sequence
        for tag, i1, i2, j1, j2 in SequenceMatcher(None, reference_queries, executed_queries).get_opcodes():
            if tag == "equal":
                # Do not display identical queries
                continue
                # for j in range(j1, j2):
                #     context = get_context_exec(j, "= ")
                #     sql = get_sql_exec(j)
                #     output.append(f"{context}\n{sql}")
            if tag in {"replace", "delete"}:
                for i in range(i1, i2):
                    context = get_context_exp(i, "- ")
                    sql = get_sql_exp(i)
                    output.append(f"{context}\n{sql}")
            if tag in {"replace", "insert"}:
                for j in range(j1, j2):
                    context = get_context_exec(j, "+ ")
                    sql = get_sql_exec(j)
                    output.append(f"{context}\n{sql}")
        return output

    def generate_message(self, nb_executed_queries):
        output = []
        if testing_settings.ASSERT_NUM_QUERIES_REFERENCE_ENABLED:
            batch_ref = self.reference and self.reference.get(self.batch_id, None)
            if batch_ref:
                # Compute diff
                ref = "> Diff with batch " + style(self.batch_id, Colors.red) + "\n\n"
                output = self.compute_diff(batch_ref["queries"], self.captured_queries)
        if not output:
            # No reference: raw results
            ref = ""
            output = [self.format_context(q, index=i) + "\n" + self.format_query(q["sql"]) for i, q in enumerate(self.captured_queries)]
        msg = (
            style(f"{nb_executed_queries} queries executed, {self.nb_expected_queries} expected\n", Colors.bold)
            + ref
            + "\n".join(output)
            + "\n----------------------------------------------------------------------\n"
        )
        return msg

    def __enter__(self):
        frame = inspect.currentframe()
        framelist = inspect.getouterframes(frame)
        self.parent_frame = framelist[1]
        self._traceback = generate_traceback(2)  # generate_traceback and __enter__ frames are ignored
        if self.test_case:
            # Retrieving index in current test method
            method = getattr(self.test_case, self.test_case._testMethodName)
            self.assert_num_queries_index = getattr(method, "_assert_num_queries_index", 0) + 1
            method.__dict__["_assert_num_queries_index"] = self.assert_num_queries_index
        super().__enter__()

    def __exit__(self, exc_type, exc_value, traceback):
        super().__exit__(exc_type, exc_value, traceback)
        # Ignore SQL queries when we got an exception: something else broke.
        if exc_type is not None:
            return
        nb_executed_queries = len(self)
        if nb_executed_queries == self.nb_expected_queries:
            # Everyhing is fine
            self.update_reference()
            return

        # INVALID COUNT
        msg = self.generate_message(nb_executed_queries)
        if self.test_case:
            if self.raise_error:
                # Exception: raising test AssertionError
                self.test_case.assertEqual(nb_executed_queries, self.nb_expected_queries, msg)
            else:
                # No exception: enqueuing error without display
                msg += style("\nWarning: assertNumQueries is configured without interruption, watch out for other test errors below", Colors.yellow)
                exc_info = (AssertNumQueriesException, AssertNumQueriesException(msg).with_traceback(self._traceback), self._traceback)
                update_testcase_outcome_error(self.test_case, exc_info)
                # Explicitly break a reference cycle: exc_info -> frame -> exc_info
                exc_info = None
        else:
            # Command line or live code debug
            if self.raise_error:
                # Exception: raising classic AssertionError
                assert nb_executed_queries == self.nb_expected_queries, msg
            else:
                # No exception: simple logger error
                logger.error(f"SmartTesting.assertNumQueries: {msg}")


class AssertNbLogsContext(_AssertLogsContext):
    def __init__(self, test_case, expected_count, logger_name, level, context=None):
        self.expected_count = expected_count
        self.context = context
        kwargs = {}
        if sys.version_info >= (3, 10):
            # Compatibility: unittest >= 3.10
            kwargs["no_logs"] = bool(expected_count == 0)
        super().__init__(test_case, logger_name, level, **kwargs)

    def __exit__(self, exc_type, exc_value, tb):
        # Note: we force "False" as exc_type when calling parent to pass through standard log control.
        super().__exit__(False, exc_value, tb)
        if exc_type is not None:
            # Let unexpected exceptions pass through
            return False
        result_count = len(self.watcher.records)
        if result_count != self.expected_count:
            msg = style(
                "Expected {} logs of level {} or higher triggered on {}, but got {}".format(
                    self.expected_count, logging.getLevelName(self.level), self.logger.name, result_count
                ),
                Colors.error,
            )
            if result_count:
                formatter = logging.Formatter(testing_settings.LOGGING_FORMATER)
                msg += style(":\n", Colors.error) + "\n".join(
                    f" {style(str(i + 1) + '.', Colors.error)} {formatter.format(record)}" for i, record in enumerate(self.watcher.records)
                )
            if self.context:
                msg += "\nContext: " + self.context
            self._raiseFailure(msg)

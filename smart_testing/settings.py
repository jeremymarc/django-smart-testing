"""
Settings for django-smart-testing are all namespaced in the SMART_TESTING setting.
For example your project's `settings.py` file might look like this:

SMART_TESTING = {
    'COLORED_OUTPUT': False,
    'LOGGING_FORMATER': "%(asctime)s %(levelname)s %(pathname)s:%(lineno)d\n%(message)s",
}

This module provides the `testing_settings` object, that is used to access
django-smart-testing settings, checking for user settings first, then falling
back to the defaults.

Inspired by rest_framework settings:
https://github.com/encode/django-rest-framework/blob/master/rest_framework/settings.py
"""
from os.path import abspath, dirname, join
from typing import List
import logging

from django.conf import settings
from django.test.signals import setting_changed

logger = logging.getLogger("smart_testing")

NETWORK_FUNCTIONS = [
    "urllib.request.urlopen",
    "requests.adapters.HTTPAdapter.send",
    "urllib.request.OpenerDirector.open",
]

DEFAULTS = {
    "ASSERT_NUM_QUERIES_FORMAT": "FULL",
    "ASSERT_NUM_QUERIES_INTERRUPT_TEST": False,
    "ASSERT_NUM_QUERIES_TIME_WARNING_THRESHOLD": 1,
    "ASSERT_NUM_QUERIES_TIME_ERROR_THRESHOLD": 5,
    "ASSERT_NUM_QUERIES_REFERENCE_ENABLED": True,
    "ASSERT_NUM_QUERIES_REFERENCE_PATH": ".smart_testing/assert_num_queries_references",
    "CAPTURE_LOGS": True,
    "CAPTURE_PRINTS": False,  # Not recommanded due to side effects on tools such as ipdb. Use loggers instead !
    "COLORED_OUTPUT": True,
    "DISABLE_NETWORK_FUNCTIONS": NETWORK_FUNCTIONS,
    "IGNORED_PATH_STACK_PATTERNS": [r".*/lib/python.*", r".*manage\.py$", r".*smart_testing/.*"],
    "LAST_FAILS_PATH": ".smart_testing/",
    "LOGGING_FORMATER": "%(asctime)s %(levelname)s %(pathname)s:%(lineno)d\n%(message)s",
    "MAX_DIFF": None,
    "POSTGRESQL_EXTENSIONS": [],
    "PRINT_NETWORK_FUNCTIONS": [
        "requests.get",
        "requests.post",
        "requests.put",
        "requests.delete",
        "requests.patch",
        # "urllib2.urlopen",
    ],
    "SERVER_DOMAIN": "django.smart.testing",
    "SERVER_NAME": "django-smart-testing",
    "SUCCESS_ASCII_FILEPATH": join(dirname(abspath(__file__)), "static", "testing", "ascii", "success.txt"),
}

REMOVED_SETTINGS: List[str] = []


class Settings:
    """
    A settings object that allows SMART_TESTING settings to be accessed as
    properties. For example:

        from smart_testing.settings import testing_settings
        print(testing_settings.COLORED_OUTPUT)

    Note:
    This is an internal class that is only compatible with settings namespaced
    under the SMART_TESTING name. It is not intended to be used by 3rd-party
    apps, and test helpers like `override_settings` may not work as expected.
    """

    def __init__(self, user_settings=None, defaults=None):
        if user_settings:
            self._user_settings = self.__check_user_settings(user_settings)
        self.defaults = defaults or DEFAULTS
        self._cached_attrs = set()

    @property
    def user_settings(self):
        if not hasattr(self, "_user_settings"):
            self._user_settings = getattr(settings, "SMART_TESTING", {})
        return self._user_settings

    def __getattr__(self, attr):
        if attr not in self.defaults:
            raise AttributeError("Invalid SMART_TESTING setting: '%s'" % attr)

        try:
            # Check if present in user settings
            val = self.user_settings[attr]
        except KeyError:
            # Fall back to defaults
            val = self.defaults[attr]

        # Cache the result
        self._cached_attrs.add(attr)
        setattr(self, attr, val)
        return val

    def __check_user_settings(self, user_settings):
        SETTINGS_DOC = "https://gitlab.com/jeremymarc/django-smart-testing/docs/"
        for setting in REMOVED_SETTINGS:
            if setting in user_settings:
                raise RuntimeError("The '%s' setting has been removed. Please refer to '%s' for available settings." % (setting, SETTINGS_DOC))
        return user_settings

    def reload(self):
        for attr in self._cached_attrs:
            delattr(self, attr)
        self._cached_attrs.clear()
        if hasattr(self, "_user_settings"):
            delattr(self, "_user_settings")

    def log(self, *args, **kwargs):
        logger.debug(*args, **kwargs)


testing_settings = Settings(None, DEFAULTS)


def reload_settings(*args, **kwargs):
    setting = kwargs["setting"]
    if setting == "SMART_TESTING":
        testing_settings.reload()


setting_changed.connect(reload_settings)

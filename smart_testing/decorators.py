import importlib
import logging
from unittest import mock

from django.utils import timezone

from colored import stylize

from smart_testing.settings import testing_settings
from smart_testing.utils import Colors, titleize, get_smart_last_traceback
from smart_testing.exceptions import DisableNetworkException

logger = logging.getLogger(__name__)


def print_network(fn):
    for _func_path in testing_settings.PRINT_NETWORK_FUNCTIONS:
        _module_getter, _func_name = mock._get_target(_func_path)
        _original = getattr(_module_getter(), _func_name)

        class _print_network_wrapper(object):
            original = staticmethod(_original)
            func_name = _func_path

            def __call__(self, *args, **kwargs):
                res = self.original(*args, **kwargs)
                logger.info(" >>>> %s  --  %s" % (self.func_name, args))
                logger.info(res)
                return res

        mocker = mock.patch(_func_path, _print_network_wrapper())
        fn = mocker(fn)
    return fn


def track(input=True, output=True):
    def decorator(callable):
        def wrapper(*args, **kwargs):
            start_time = timezone.now()
            if input:
                logger.info(" =>> %s\n*%s **%s", callable.__name__, args, kwargs)
            result = callable(*args, **kwargs)
            if output:
                end_time = timezone.now()
                logger.info(" <<= %s (%s)\n%s", callable.__name__, end_time - start_time, result)
            return result

        return wrapper

    return decorator


def disable_network(fn):
    """
    Block any external request and print calling info.
    Additionnal external call functions can be added to DISABLE_NETWORK_FUNCTIONS in smart_testing settings.

    To display info without blocking the call use `print_network`.

    Usage:
        @disable_network
        def test_case(self):
            ...
    """
    for net_fn in testing_settings.DISABLE_NETWORK_FUNCTIONS:

        class _raise_network_error(object):
            _net_fn = net_fn

            def get_traceback(self):
                return get_smart_last_traceback(prompt="")

            def __call__(self, *args, **kwargs):
                from requests.models import PreparedRequest
                from urllib.request import Request

                style = stylize if testing_settings.COLORED_OUTPUT else lambda msg, x: msg  # TODO: should use common.utils.style
                callback = self._net_fn
                origin = self.get_traceback()
                url = None
                headers = "<unknown headers>"
                final_args = []
                for arg in args:
                    if isinstance(arg, PreparedRequest):
                        url = f"{arg.method} {arg.url}"
                        headers = arg.headers
                    elif isinstance(arg, Request):
                        url = f"{arg.get_method()} {arg.get_full_url()}"
                        headers = arg.headers
                    else:
                        if isinstance(arg, str) and url is None:
                            url = arg
                        else:
                            final_args.append(arg)
                url = "<unknown url>" if url is None else url
                message = (
                    "\n\n" + style(titleize("Trying to access network during tests", separator="/!\\ "), Colors.error) + "\n\n"
                    f"{url}\n"
                    f" - Origin: {origin}\n"
                    f" - Callback: {callback}\n"
                    f" - Args: {final_args}\n"
                    f" - Kwargs: {kwargs}\n"
                    f" - Headers: {headers}\n"
                )
                raise DisableNetworkException(message)

        mocker = mock.patch(net_fn, _raise_network_error())
        fn = mocker(fn)
    return fn
